import telethon
from telethon import TelegramClient, events

api_id = 21959202
api_hash = '9366b1f72b22e9385438bc0ad9da646e'

client = TelegramClient('session_name', api_id, api_hash)

@client.on(events.NewMessage(pattern='/start'))
async def start_handler(event):
    await event.respond('Hi there! Send me a message and I will repeat it back to you.')

@client.on(events.NewMessage)
async def repeat_handler(event):
    await event.respond(event.message.message)

client.start()
client.run_until_disconnected()